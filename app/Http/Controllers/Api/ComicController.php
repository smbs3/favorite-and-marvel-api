<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class ComicController extends Controller
{
    public function  generateURL ($category)
    {
        $publicKey =  env('MARVEL_PUBLIC_KEY');
        $privateKey = env('MARVEL_PRIVATE_KEY');
        $url = env('MARVEL_URL');

        $ts = time();
        $hash = md5($ts . $privateKey . $publicKey);

        $client = new Client();
        $response = $client->request('GET', $url. $category, ['query' => ['ts' => $ts,'apikey' => $publicKey,  'hash' => $hash,],]);
        return $response;
    }

    public function getComics()
    {
        $comics= $this->generateURL('comics?');
        $result = json_decode($comics->getBody()->getContents(), true);
        return response()->json(
            $result
        );
    }

    public function showComics($id){
        $comics= $this->generateURL("comics/$id?");
        

        $result = json_decode($comics->getBody()->getContents(), true);
        return response()->json(
            $result
        );
    }

    public function showComicsCharacters($id){
        $comics= $this->generateURL("comics/$id/comics?");
        

        $result = json_decode($comics->getBody()->getContents(), true);
        return response()->json(
            $result
        );
    }
}
