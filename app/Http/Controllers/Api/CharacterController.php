<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class CharacterController extends Controller
{
    public function  generateURL ($category)
    {
        $publicKey =  env('MARVEL_PUBLIC_KEY');
        $privateKey = env('MARVEL_PRIVATE_KEY');
        $url = env('MARVEL_URL');

        $ts = time();
        $hash = md5($ts . $privateKey . $publicKey);

        $client = new Client();
        $response = $client->request('GET', $url. $category, ['query' => ['ts' => $ts,'apikey' => $publicKey,  'hash' => $hash,],]);
        return $response;
    }

    public function getCharacters()
    {
        $characters= $this->generateURL('characters?');
        $result = json_decode($characters->getBody()->getContents(), true);
        return response()->json(
            $result
        );
    }

    public function showCharacters($id){
        $characters= $this->generateURL("characters/$id?");
        

        $result = json_decode($characters->getBody()->getContents(), true);
        return response()->json(
            $result
        );
    }

    public function showCharactersComics($id){
        $characters= $this->generateURL("characters/$id/comics?");
        

        $result = json_decode($characters->getBody()->getContents(), true);
        return response()->json(
            $result
        );
    }
}