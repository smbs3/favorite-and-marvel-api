<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveFavoriteRequest;
use App\Models\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller

{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        return Auth::user()->favorites;
    }

    /**
     * Store a newly created resource in storage.
     */

    public function store(SaveFavoriteRequest $request)
    {

        $favorite = new Favorite();
        $favorite->marvel_id = $request->marvel_id;
        $favorite->category = $request->category;
        $user = auth()->user();
        $user->favorites()->save($favorite);

        return response()->json([
            'res' => true,
            'msg' => 'registered favorite'
        ], 200);
    }

    /**
     * Display the specified resource.
     */

    public function show(Favorite $favorite)
    {
        return response()->json([
            'res' => true,
            'msg' => $favorite
        ], 200);
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        return response()->json([
            'res' => true,
            'msg' => 'se mostraron todos los favoritos'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Favorite $favorite)
    {
        $favorite->delete();
        return response()->json([
            'res' => true,
            'msg' => 'Favorito eliminado correctamente'
        ], 200);
    }
}
