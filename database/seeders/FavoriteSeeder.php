<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FavoriteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('favorites')->insert([
            [
                'category' => 'COMIC',
                'user_id' => 1,
                'marvel_id' => 14,
            ],
            [

                'category' => 'COMIC',
                'user_id' => 2,
                'marvel_id' => 14,
            ],
            [

                'category' => 'CHARACTER',
                'user_id' => 2,
                'marvel_id' => 1017100,
            ],
            [
                'category' => 'CHARACTER',
                'user_id' => 3,
                'marvel_id' => 1011334,
            ],
        ]);
    }
}
