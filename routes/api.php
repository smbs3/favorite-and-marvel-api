<?php

use App\Http\Controllers\Api\AuthenticateController;
use App\Http\Controllers\Api\CharacterController;
use App\Http\Controllers\Api\FavoriteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::post('/register', [AuthenticateController::class, 'register']);
Route::post('/login', [AuthenticateController::class, 'login']);

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/logout', [AuthenticateController::class, 'logout']);
    Route::apiResource('favorites', FavoriteController::class);
    // Route::post('/favorites', [FavoriteController::class,'store'] );
});

Route::get('/characters', [CharacterController::class,'getCharacters']);
Route::get('/characters/{id}', [CharacterController::class,'showCharacters']);
Route::get('/characters/{id}/comics', [CharacterController::class,'showCharactersComics']);

Route::get('/comics', [CharacterController::class,'getCharacters']);
Route::get('/comics/{id}', [CharacterController::class,'showCharacters']);
Route::get('/comics/{id}/characters', [CharacterController::class,'showCharactersComics']);